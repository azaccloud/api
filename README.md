# AzacCloud API

Bu proje AzacCLoud'un bileşenlerinin birbirleri ile iletişimini sağlayan bir API projesidir.

## Endpoint'leri

Endpoint'ler AzacCloud'da temel olarak /token endpoint'i ve bu endpoint arkasına gelen cloud bileşenlerini yönetmeyi sağlayan endpoint'ler bulunur.

 - **/token** : kullanıcı doğrulama aşamasında kullancı adı parolaya göre postgresql'de veri arar bulursa o satırda kullanıcı token bilgisini alır. (Not: Token bilgisi sabit tutulmamalı zaman aşımına uğramalı veya logout dendiğinde değişmeli)
   - **/network** : token bilgisi alındıktan sonra sanal ağ oluşturma, firewall gibi işlemleri bu endpoint üzerinden istek göndererek gerçekleştirilir.
   - **/storage** : block, obs veya nfs gibi depolama çözümleri ile ilgili her işlem bu endpoint üzerinden gider.
   - **/compute** : kubernetes,container ve vm gibi aygıtların tüm işlemleri bu endpoint üzerinden gider
   - **/database** : rdbs veya nosql veritabanı hizmeti ile ilgili tüm işlemler bu endpoint üzerinden kulanılır.
   - **/security** : Kullanıcı ile ilgili işlemler bu endpoint üzerinden gerçekleşir.


